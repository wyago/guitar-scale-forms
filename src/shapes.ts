import { h } from "maquette";

export function line(x1: number, x2: number, y1: number, y2: number, more?: {}) {
    return h("line", { x1: String(x1), x2: String(x2), y1: String(y1), y2: String(y2), stroke: "black", ...more || {} });
}