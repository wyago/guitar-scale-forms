import { ScaleDegree } from './music';
export enum Note {
    A,
    Asharp,
    B,
    C,
    Csharp,
    D,
    Dsharp,
    E,
    F,
    Fsharp,
    G,
    Gsharp
}

export const noteNames = {
    [Note.A]: "A",
    [Note.Asharp]: "A#",
    [Note.B]: "B",
    [Note.C]: "C",
    [Note.Csharp]: "C#",
    [Note.D]: "D",
    [Note.Dsharp]: "D#",
    [Note.E]: "E",
    [Note.F]: "F",
    [Note.Fsharp]: "F#",
    [Note.G]: "G",
    [Note.Gsharp]: "G#",
}

export function noteName(note: Note) {
    return noteNames[note];
}

export type Key = { __KEY_TAG: true } & Note;

export enum Quality {
    DoubleFlat = 2,
    Flat = -1,
    Perfect = 0,
    Sharp = 1,
    DoubleSharp = 2
}

export interface FreeChord {
    readonly root: Note;
    readonly qualities: Quality[];
}

export type ScaleDegree = { __SCALE_DEGREE_TAG: true } & number;
export const tonic = scaleDegree(1);

export interface KeyChord {
    readonly root: ScaleDegree;
    readonly borrow: Mode;
}

export interface Pitch {
    readonly note: Note;
    readonly octave: number;
}

export function scaleDegree(value: number): ScaleDegree {
    return value as ScaleDegree;
}

export function key(note: Note): Key {
    return note as Key;
}

export function freeChord(root: Note, qualities: Quality[]): FreeChord {
    return { root, qualities };
}

export function keyChord(root: ScaleDegree, borrow: Mode): KeyChord {
    return { root, borrow };
}

export function pitch(note: Note, octave: number): Pitch {
    while (note > 12) {
        note -= 12;
    }
    while (note < 0) {
        note += 12;
    }
    return { note, octave };
}

function rotate<T>(array: T[], n: number): T[] {
    return array.map((_, i) => array[(i + n) % array.length]);
}

export const whole = 2;
export const half = 1;

export type Mode = [1 | 2, 1 | 2, 1 | 2, 1 | 2, 1 | 2, 1 | 2, 1 | 2];
export const ionian: Mode = [whole, whole, half, whole, whole, whole, half];
export const dorian = rotate(ionian, 6) as Mode;
export const phrygian = rotate(ionian, 5) as Mode;
export const lydian = rotate(ionian, 4) as Mode;
export const mixolydian = rotate(ionian, 3) as Mode;
export const aeolian = rotate(ionian, 2) as Mode;
export const locrian = rotate(ionian, 1) as Mode;

export const modes = [ionian, dorian, phrygian, lydian, mixolydian, aeolian, locrian];
export const modeNames = ["Ionian (major)", "Dorian", "Phrygian", "Lydian", "Mixolydian", "Aeolian (minor)", "Locrian"]

export function getDegreeIn(key: Key, mode: Mode, note: Note): ScaleDegree | undefined {
    let start: Note = key;
    let i = scaleDegree(1);
    for (const step of mode) {
        if (start === note) {
            return i;
        }
        start += step;
        if (start > Note.Gsharp) {
            start -= 12;
        }
        i = scaleDegree(i + 1);
    }
    if (start === note) {
        return i;
    }
    return undefined;
}