import { ScaleDegree } from './../model/music';
import { h } from 'maquette';
import { line } from '../shapes';

let nextKey = 0;

export function svgScaleDegree(scaleDegree: ScaleDegree, highlight?: boolean) {
    return (x: number, y: number) => [
        h("circle", { key: ++nextKey, cx: String(x), cy: String(y), r: "15", fill: highlight ? "#fee" : "white", stroke: "#eee" }, [String(scaleDegree)]),
        h("text", { key: ++nextKey, x: String(x - 5), y: String(y + 7) }, [String(scaleDegree)]),
        line(x - 3, x, y - 7, y - 10, { key: ++nextKey }),
        line(x + 3, x, y - 7, y - 10, { key: ++nextKey }),
    ];
}

export function svgQuestionScaleDegree(scaleDegree: ScaleDegree) {
    return (x: number, y: number) => [
        h("circle", { key: ++nextKey, cx: String(x), cy: String(y), r: "15", fill: "white", stroke: "#eee" }, [String(scaleDegree)]),
        h("text", { key: ++nextKey, x: String(x - 5), y: String(y + 7) }, ["?"]),
        line(x - 3, x, y - 7, y - 10, { key: ++nextKey }),
        line(x + 3, x, y - 7, y - 10, { key: ++nextKey }),
    ];
}