import { VNode } from 'maquette';
import { ScaleDegree, Key, Mode, getDegreeIn, scaleDegree, key, pitch, Note, noteNames, tonic } from './../model/music';
import { h } from 'maquette';
import { line } from '../shapes';
import { Tuning, FretRender, guitar } from './guitar';
import { svgScaleDegree } from './scaleDegree';


function scaleFrets(key: Key, mode: Mode, tuning: Tuning, chord: number): FretRender[] {
    const result: FretRender[] = [];

    const chordDegrees = 
        [chord, chord + 2, chord + 4, chord + 6]
        .map(x => x > 7 ? x - 7 : x);

    for (let string = 5; string >= 0; --string) {
        let fret = 0;
        let degree = getDegreeIn(key, mode, tuning[string].note);
        if (!degree) {
            fret += 1;
            degree = getDegreeIn(key, mode, (tuning[string].note + 1) % 12)!;
        }

        while (fret <= 13) {
            result.push({
                fret: { string, fret },
                render: svgScaleDegree(degree!, chordDegrees.indexOf(degree) !== -1)
            });
            fret += mode[degree! - 1];
            degree = scaleDegree(degree! + 1);
            if (degree! >= 8) {
                degree = tonic;
            }
        }
    }

    return result;
}

export function scale(key: Key, mode: Mode, tuning: Tuning, chord: number): VNode {
    return guitar({
        fretRenders: scaleFrets(key, mode, tuning, chord),
        tuning
    });
}
