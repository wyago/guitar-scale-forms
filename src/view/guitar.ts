import { Pitch, noteName } from './../model/music';
import { h, VNode } from "maquette";
import { line } from "../shapes";
import { by } from '../functional';

export interface Fret {
    readonly string: number;
    readonly fret: number;
}

export type Tuning = [Pitch, Pitch, Pitch, Pitch, Pitch, Pitch];

export type FretRender = {
    fret: Fret, 
    render: (x: number, y: number) => (VNode | VNode[])
};

export interface GuitarProps {
    readonly fretRenders: FretRender[];
    readonly tuning: Tuning;
}

export function guitar({ fretRenders, tuning }: GuitarProps) {
    const fretboardTop = 25;
    const stringDistance = 40;
    const fretboardBottom = stringDistance * 5 + fretboardTop;

    const fretDistance = 70;
    const fretExtension = 5;

    // Includes a virtual fret for the empty string.
    const fretCount = 13;

    const fretboardLeft = 85;
    const fretboardRight = fretboardLeft + fretCount * fretDistance;

    const strings = by(fretboardTop, stringDistance, 6).map(y => 
        line(fretboardLeft, fretboardRight, y + 0.5, y + 0.5), { stroke: "#A98" });

    const frets = by(fretboardLeft, fretDistance, fretCount).map((x, i) => 
        line(x + 0.5, x + 0.5, 
            fretboardTop - (i === 0 ? fretExtension : 0), 
            fretboardBottom + (i === 0 ? fretExtension : 0), { 
                stroke: "#765", 
                "stroke-width": "3" 
            }));

    const flourishes = [3, 5, 7, 9, 12].map(x =>
        x % 3 !== 0 ?
            h("circle", {
                cx: String(x * fretDistance + fretboardLeft - fretDistance * 0.5),
                cy: String(fretboardTop + stringDistance * 2.5),
                r: String("10"),
                fill: "#999"
            }) :
            [h("circle", {
                cx: String(x * fretDistance + fretboardLeft - fretDistance * 0.5),
                cy: String(fretboardTop + stringDistance * 1.5),
                r: String("10"),
                fill: "#999"
            }), h("circle", {
                cx: String(x * fretDistance + fretboardLeft - fretDistance * 0.5),
                cy: String(fretboardTop + stringDistance * 3.5),
                r: String("10"),
                fill: "#999"
            })]);

    const fretNumbers = by(fretboardLeft, fretDistance, fretCount).map((x, i) => 
        h("text",
            { x: String(x - 28.5), y: String(fretboardTop - 10), "font-size": "14px" }, 
            [String(i)]));

    const tunings = tuning.map((pitch, i) => 
        h("text",
            { x: String(0), y: String(fretboardTop + 6 + stringDistance * i) }, 
            [noteName(pitch.note) + pitch.octave]));

    return h("svg", { width: "1000", height: "250" }, [
        h("rect", { 
            x: String(fretboardLeft), 
            y: String(fretboardTop), 
            width: String(fretboardRight - fretboardLeft), 
            height: String(fretboardBottom - fretboardTop), 
            fill: "#f3f0ef"
        }),
        ...strings,
        ...frets,
        ...tunings,
        ...fretNumbers,
        ...flourishes,
        ...fretRenders.map(({ fret: { string, fret }, render}) =>
            render(fretboardLeft - fretDistance * 0.5 + fretDistance * fret, fretboardTop + stringDistance * string))
    ]);
}

document.getElementsByTagName