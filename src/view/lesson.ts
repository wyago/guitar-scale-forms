import { Key, Mode, getDegreeIn, scaleDegree, tonic, ScaleDegree } from './../model/music';
import { guitar, FretRender, Tuning, Fret } from './guitar';
import { svgScaleDegree, svgQuestionScaleDegree } from './scaleDegree';
import { VNode, h } from 'maquette';
import { by } from '../functional';

function separateTonics(key: Key, mode: Mode, tuning: Tuning): { tonics: Fret[], degrees: [Fret, ScaleDegree][] } {
    const tonics: Fret[] = [];
    const degrees: [Fret, ScaleDegree][] = [];

    for (let string = 5; string >= 0; --string) {
        for (let fret = 0; fret < 5; ++fret) {
            let degree = getDegreeIn(key, mode, (tuning[string].note + fret) % 12);
            if (degree === tonic) {
                tonics.push({ string, fret });
            } else if (degree) {
                degrees.push([{ string, fret }, degree]);
            }
        }
    }

    return { tonics, degrees };
}

interface State {
    testTonic: Fret;
    testDegreeFret: Fret;
    testDegree: ScaleDegree;
    renders: FretRender[];
    disabled: { [degree: number]: boolean };
}

function setup(
        tonics: Fret[],
        degrees: [Fret, ScaleDegree][],
        set: (state: State) => void): void {
    const testTonic = tonics[Math.floor(Math.random() * tonics.length)];
    const [testDegreeFret, testDegree] = degrees[Math.floor(Math.random() * degrees.length)]

    const renders: FretRender[] = [];
    renders.push({ fret: testTonic, render: svgScaleDegree(tonic) });
    renders.push({ fret: testDegreeFret, render: svgQuestionScaleDegree(testDegree) });
    
    set({
        testTonic,
        testDegree,
        testDegreeFret,
        renders,
        disabled: [],
    });
}

const makeOnclick = (
        state: () => State, 
        moveOn: () => void,
        disable: (degree: ScaleDegree) => void,
        degree: ScaleDegree) => () => {
    if (state().testDegree === degree) {
        moveOn();
    } else {
        disable(degree);
    }
}

export function lesson(key: Key, mode: Mode, tuning: Tuning): () => VNode[] {
    const { tonics, degrees } = separateTonics(key, mode, tuning);

    let state: State | undefined = undefined;
    const set = (next: State) => state = next;
    setup(tonics, degrees, set);

    const inputs = by(1, 1, 7).map(scaleDegree).map(degree => {
        return h("button", {
            disabled: state!.disabled[degree],
            onclick: makeOnclick(
                () => state!,
                () => setup(tonics, degrees, set),
                degree => state!.disabled[degree] = true, degree) 
        }, [String(degree)]);
    });

    return () => [
        ...inputs,
        guitar({
            fretRenders: state!.renders,
            tuning
        })
    ];
}
