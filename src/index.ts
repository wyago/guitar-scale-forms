import * as redux from 'redux';
import { h, createProjector, VNode } from 'maquette';
import { guitar, FretRender, Tuning } from './view/guitar';
import { Note, pitch, ScaleDegree, scaleDegree, Mode, ionian, Key, key, getDegreeIn, KeyChord, mixolydian, noteName, noteNames, modeNames, modes } from './model/music';
import { line } from "./shapes";
import { scale } from './view/scale';
import { lesson } from './view/lesson';
import { by } from './functional';

let uiKey = key(Note.E);
let uiModeName = modeNames[0];
let uiChord = 1;

function onKeyChange(event: Event) {
    uiKey = key(Number((event!.target! as HTMLSelectElement).value!));
    configure();
}

function onModeChange(event: Event) {
    uiModeName = (event.target! as HTMLSelectElement).value!;
    configure();
}

function onChordChange(event: Event) {
    uiChord = Number((event.target! as HTMLSelectElement).value!);
    configure();
}

const tuning: Tuning = [
    pitch(Note.E, 4),
    pitch(Note.B, 3),
    pitch(Note.G, 3),
    pitch(Note.D, 3),
    pitch(Note.A, 2),
    pitch(Note.E, 2),
];

const chords = ["I7", "ii7", "iii7", "IV7", "V7", "vi7", "vii°7"];

let lessoner: () => VNode[];

function primaryRender(): VNode {
    return h("div.container", [
        h("h1", ["Guitar scale forms"]),
        h("div.controls", [
            h("label", [
                "Key:",
                h("select", { onchange: onKeyChange },
                    Object.keys(noteNames).map(note =>
                        h("option", {
                            value: note,
                            selected: key(Number(note)) == uiKey,
                        }, [
                            noteName(note as any)
                        ])))]),
            h("label", ["Mode:", h("select", { onchange: onModeChange },
                modeNames.map(name =>
                    h("option", {
                        value: name,
                        selected: name === uiModeName,
                    }, [name])))]),
            h("label", ["Mode:", h("select", { onchange: onChordChange },
                by(0, 1, 7).map(number =>
                    h("option", {
                        value: String(number + 1),
                        selected: number + 1 === uiChord,
                    }, [chords[number]])))]),
        ]),
        scale(uiKey, modes[modeNames.indexOf(uiModeName)], tuning, uiChord)
    ]);
}

let projector = createProjector();

function configure() {
    lessoner = lesson(uiKey, modes[modeNames.indexOf(uiModeName)], tuning);
}
configure();
projector.append(document.body, primaryRender);