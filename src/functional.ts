

export function by(start: number, delta: number, count: number) {
    const result = [];
    for (let i = 0; i < count; ++i) {
        result.push(start);
        start += delta;
    }
    return result;
}
